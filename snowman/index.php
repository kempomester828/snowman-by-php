<?php
$img = imagecreatetruecolor(600, 1200);

$white = imagecolorallocate($img, 255, 255, 255);
$red   = imagecolorallocate($img, 255,   0,   0);
$grey = imagecolorallocate($img,   40, 40,   40);
$orange  = imagecolorallocate($img,   210,   105, 30);
$brown  = imagecolorallocate($img,   139,   69, 19);
//head start
imagefilledarc($img, 300, 200, 200, 200,  0, 360, $white, IMG_ARC_EDGED);
// mouth
imagefilledarc($img,  240,  245,  30,  30,  0, 360, $grey, IMG_ARC_EDGED);
imagefilledarc($img,  270,  260,  30,  30,  0, 360, $grey, IMG_ARC_EDGED);
imagefilledarc($img,  300,  270,  30,  30,  0, 360, $grey, IMG_ARC_EDGED);
imagefilledarc($img,  330,  260,  30,  30,  0, 360, $grey, IMG_ARC_EDGED);
imagefilledarc($img,  360,  245,  30,  30,  0, 360, $grey, IMG_ARC_EDGED);
//nose
imagefilledarc($img,  335,  230,  90,  90,  200, 230, $orange, IMG_ARC_PIE);
// left and then the right eye
imagefilledarc($img,  260,  175,  50,  50,  0, 360, $grey, IMG_ARC_EDGED);
imagefilledarc($img, 340,  175,  50,  50,  0, 360, $grey, IMG_ARC_EDGED);
//hat
imagefilledrectangle($img, 120, 110, 480, 80, $red);
imagefilledrectangle($img, 400, 30, 200, 100, $red);
//head end
//body start
imagefilledarc($img, 300, 440, 300, 300,  0, 360, $white, IMG_ARC_EDGED);
//buttons
imagefilledarc($img,  300,  370,  30,  30,  0, 360, $grey, IMG_ARC_EDGED);
imagefilledarc($img,  300,  410,  30,  30,  0, 360, $grey, IMG_ARC_EDGED);
imagefilledarc($img,  300,  450,  30,  30,  0, 360, $grey, IMG_ARC_EDGED);
imagefilledarc($img,  300,  490,  30,  30,  0, 360, $grey, IMG_ARC_EDGED);
//arms
//left
imagefilledrectangle($img, 40, 420, 200, 440, $brown);
imagefilledrectangle($img, 40, 300, 60, 440, $brown);
imagefilledrectangle($img, 20, 290, 80, 300, $brown);
imagefilledrectangle($img, 20, 270, 30, 290, $brown);
imagefilledrectangle($img, 45, 270, 55, 290, $brown);
imagefilledrectangle($img, 70, 270, 80, 290, $brown);
//right
imagefilledrectangle($img, 560, 420, 400, 440, $brown);
imagefilledrectangle($img, 560, 300, 540, 440, $brown);
imagefilledrectangle($img, 580, 290, 520, 300, $brown);
imagefilledrectangle($img, 580, 270, 570, 290, $brown);
imagefilledrectangle($img, 555, 270, 545, 290, $brown);
imagefilledrectangle($img, 530, 270, 520, 290, $brown);
//body end
//footer start
imagefilledarc($img, 300, 740, 400, 400,  0, 360, $white, IMG_ARC_EDGED);
//footer end

header("Content-type: image/png");
imagepng($img);

imagedestroy($img);
?>
